export const bodyAreas = [
  {
    id: "0",
    name: "Abs",
    ico: "abs",
  },
  {
    id: "1",
    name: "Arms",
    ico: "arms",
  },
  {
    id: "2",
    name: "Back",
    ico: "back",
  },
  {
    id: "3",
    name: "Biceps",
    ico: "biceps",
  },
  {
    id: "4",
    name: "Calves",
    ico: "calves",
  },
  {
    id: "5",
    name: "Chest",
    ico: "chest",
  },
  {
    id: "6",
    name: "Core",
    ico: "core",
  },
  {
    id: "7",
    name: "Glutes",
    ico: "glutes",
  },
  {
    id: "8",
    name: "Hamstrings",
    ico: "hamstrings",
  },
  {
    id: "9",
    name: "Legs",
    ico: "legs",
  },
  {
    id: "10",
    name: "Quads",
    ico: "quads",
  },
  {
    id: "11",
    name: "Shoulders",
    ico: "shoulders",
  },
  {
    id: "12",
    name: "Triceps",
    ico: "triceps",
  },
];
