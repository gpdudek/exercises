import React, { useState } from "react";
import Image from "next/image";
import PropTypes from "prop-types";

import Button from "../components/Button";
import CustomExercisesList from "../components/CustomExercisesList";
import ButtonIcon from "../components/ButtonIcon";

import { useLocalStorage } from "../hooks/useLocalStorage";

import { bodyAreas } from "../databases/bodyAreas.js";

import style from "../styles/Home.module.css";

export default function Home({ exercises }) {
  const [picture, setPicture] = useLocalStorage("picture", "womens");
  const [activeBodyArea, setActiveBodyArea] = useState();

  const getExercisesList = () => {
    let newExercises = [...exercises];

    if (activeBodyArea) {
      newExercises = newExercises.filter((exercise) =>
        exercise.bodyAreas.includes(activeBodyArea)
      );
    }

    return newExercises;
  };

  return (
    <main>
      <div className={style.menu}>
        <div className={style.buttonContainer}>
          <Button
            onClick={() => setPicture("womens")}
            size="large"
            noBorder
            active={picture === "womens"}
          >
            womens
          </Button>
          <Image
            src={"/gymshark.svg"}
            width={30}
            height={30}
            className={style.logo}
          />
          <Button
            onClick={() => setPicture("mens")}
            size="large"
            noBorder
            active={picture === "mens"}
          >
            mens
          </Button>
        </div>
        <div className={style.iconsContainer}>
          {bodyAreas.map((bodyArea) => (
            <ButtonIcon
              key={bodyArea.id}
              onClick={() => setActiveBodyArea(bodyArea.name)}
              src={`/${bodyArea.ico}.png`}
              alt={bodyArea.name}
              text={bodyArea.name}
              active={activeBodyArea === bodyArea.name}
            />
          ))}
          <Button
            onClick={() => setActiveBodyArea()}
            rounded
            black
            size="small"
          >
            clear
          </Button>
        </div>
      </div>
      <CustomExercisesList exercises={getExercisesList()} picture={picture} />
    </main>
  );
}

export async function getServerSideProps() {
  const res = await fetch(
    "https://private-922d75-recruitmenttechnicaltest.apiary-mock.com/customexercises/"
  );
  const data = await res.json();

  return {
    props: {
      exercises: data.exercises,
    },
  };
}

Home.propTypes = {
  exercises: PropTypes.array.isRequired,
};
