import React from "react";
import Image from "next/image";
import Head from "next/head";
import PropTypes from "prop-types";

import Button from "../../components/Button";
import Pill from "../../components/Pill";

import { useLocalStorage } from "../../hooks/useLocalStorage";

import style from "./exercises.module.css";

export default function Exercise({ exercise }) {
  const [picture, setPicture] = useLocalStorage("picture", "womens");

  const handleWomenPic = () => {
    setPicture("womens");
  };

  const handleMensPic = () => {
    setPicture("mens");
  };

  return (
    <div>
      <Head>
        <title>{exercise.name}</title>
      </Head>
      <main className={style.container}>
        <Button href="/" size="small" rounded className={style.buttonBack}>
          Back
        </Button>
        <div className={style.image}>
          <Image
            src={
              picture === "womens" ? exercise.female.image : exercise.male.image
            }
            width={700}
            height={700}
          />

          <div className={style.buttonsWrapper}>
            <Button onClick={handleWomenPic} size="small" black rounded>
              womens
            </Button>
            <Button onClick={handleMensPic} size="small" black rounded>
              mens
            </Button>
          </div>
        </div>
        <div>
          <h1>{exercise.name}</h1>
          <div>
            {exercise.bodyAreas.map((bodyArea) => (
              <Pill key={bodyArea}>{bodyArea}</Pill>
            ))}
          </div>
          <div dangerouslySetInnerHTML={{ __html: exercise.transcript }} />
        </div>
      </main>
    </div>
  );
}

export async function getServerSideProps({ params }) {
  const res = await fetch(
    "https://private-922d75-recruitmenttechnicaltest.apiary-mock.com/customexercises/"
  );
  const customexercises = await res.json();

  return {
    props: {
      exercise: customexercises.exercises.find(
        (exercise) => exercise.id === params.id
      ),
    },
  };
}

Exercise.propTypes = {
  exercise: PropTypes.object.isRequired,
};
