import React from "react";
import clsx from "clsx";
import Link from "next/link";
import PropTypes from "prop-types";

import style from "./button.module.css";

export default function Button({
  component: Component = "button",
  href = null,
  onClick = null,
  children,
  size = "medium",
  noBorder = false,
  rounded = false,
  disabled = false,
  className,
  borderRadius = false,
  black = false,
  active = false,
  ...otherprops
}) {
  const classNames = clsx(
    style.normal,
    noBorder && style.noBorder,
    rounded && style.rounded,
    size === "small" && style.small,
    size === "medium" && style.medium,
    size === "large" && style.large,
    disabled && style.disabled,
    borderRadius && style.borderRadius,
    black && style.black,
    active && style.active,
    className
  );

  return href ? (
    <Link href={href}>
      <a className={classNames} {...otherprops}>
        {children}
      </a>
    </Link>
  ) : (
    <Component onClick={onClick} className={classNames} {...otherprops}>
      {children}
    </Component>
  );
}

Button.propTypes = {
  component: PropTypes.string,
  href: PropTypes.string,
  onClick: PropTypes.func,
  size: PropTypes.oneOf(["small", "medium", "large"]),
  noBorder: PropTypes.bool,
  rounded: PropTypes.bool,
  disabled: PropTypes.bool,
  className: PropTypes.string,
  borderRadius: PropTypes.bool,
  black: PropTypes.bool,
  active: PropTypes.bool,
};
