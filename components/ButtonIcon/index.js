import React from "react";
import clsx from "clsx";
import PropTypes from "prop-types";

import style from "./buttonIcon.module.css";

export default function ButtonIcon({
  src,
  onClick,
  alt = "Body area icon",
  className,
  text,
  active,
  ...otherprops
}) {
  return (
    <button
      onClick={onClick}
      className={clsx(style.container, className, active && style.active)}
    >
      <img src={src} alt={alt} className={style.ico} {...otherprops} />
      <span className={style.text}>{text}</span>
    </button>
  );
}

ButtonIcon.propTypes = {
  src: PropTypes.string.isRequired,
  onClick: PropTypes.func.isRequired,
  alt: PropTypes.string,
  className: PropTypes.string,
  text: PropTypes.string.isRequired,
  active: PropTypes.string,
};
