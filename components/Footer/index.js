import React from "react";

import style from "./footer.module.css";

export default function Footer() {
  return (
    <footer className={style.footer}>
      <a href="https://gosia.pnd.io/" target="_blank" rel="author">
        Powered by Gosia Palys-Dudek "Let me make your idea clickable!"🧞‍♀️
      </a>
    </footer>
  );
}
