import React from "react";
import Image from "next/image";
import Link from "next/link";
import PropTypes from "prop-types";

import Pill from "../Pill";
import Button from "../Button";

import style from "./exerciseCard.module.css";

export default function ExerciseCard({ exercise, picture }) {
  return (
    <div>
      <Link href={`/exercises/${exercise.id}`}>
        <a className={style.item} key={exercise.id}>
          <Image
            src={
              picture === "womens" ? exercise.female.image : exercise.male.image
            }
            className={style.image}
            width={350}
            height={350}
          />

          <div className={style.textContainer}>
            <p className={style.title}>{exercise.name}</p>
            <div className={style.pills}>
              {exercise.bodyAreas.map((bodyArea) => (
                <Pill key={bodyArea}>{bodyArea}</Pill>
              ))}
            </div>
            <Button component="div" size="medium" borderRadius>
              Check it out!
            </Button>
          </div>
        </a>
      </Link>
    </div>
  );
}

ExerciseCard.propTypes = {
  exercise: PropTypes.object.isRequired,
  picture: PropTypes.string.isRequired,
};
