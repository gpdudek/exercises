import React from "react";
import clsx from "clsx";
import PropTypes from "prop-types";

import style from "./pill.module.css";

export default function Pill({ className, children, ...otherprops }) {
  return (
    <span className={clsx(style.container, className)} {...otherprops}>
      {children}
    </span>
  );
}

Pill.propTypes = {
  className: PropTypes.string,
};
