import React from "react";
import PropTypes from "prop-types";

import ExerciseCard from "../ExerciseCard";

import style from "./customExeList.module.css";

export default function CustomExercisesList({ exercises, picture }) {
  return (
    <ul className={style.items}>
      {exercises.map((exercise) => (
        <li key={exercise.id}>
          <ExerciseCard exercise={exercise} picture={picture} />
        </li>
      ))}
    </ul>
  );
}

CustomExercisesList.propTypes = {
  exercises: PropTypes.array.isRequired,
  picture: PropTypes.string.isRequired,
};
